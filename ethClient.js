'use strict';

module.exports = Web3 => ({
    web3http: new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545')),
    web3: new Web3(new Web3.providers.WebsocketProvider('ws://127.0.0.1:8546'))
})