'use strict';
var fetch = require('node-fetch');

module.exports = web3 => {
    const accounts = [
        '0x4DbE965AbCb9eBc4c6E9d95aEb631e5B58E70d5b'.toLowerCase(),
        '0x5136a9A5D077aE4247C7706b577F77153C32A01C'.toLowerCase(),
        '0x0BFb53cBF2A5481720fe364Aa7aFd65d9F9183bc'.toLowerCase(),
        '0x198E18EcFdA347c6cdaa440E22b2ff89eaA2cB6f'.toLowerCase(),
        '0x104B2618231020b4320580b40FE05dce77323fa1'.toLowerCase(),
        '0x6b175474e89094c44da98b954eedeac495271d0f'.toLowerCase(),
        '0x642bc38686BabED5aF302ED59d4eE0F911E5B850'.toLowerCase(),
        '0xA62c5bA4D3C95b3dDb247EAbAa2C8E56BAC9D6dA'.toLowerCase()
    ];

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'key=AAAAxZOz1Es:APA91bEsjlSxgg8UYDlOScvtlvVyjanrtflSWzsAyVNXcX64fyq9W1z6G9t7eYk97NJIklbqfmZ_R-Ik4aFm8TQgjjv6d0AQmg6pdbOMo8utL6i3p80X9o6ZhVrEyNVgywWOq0jiq5EO'
    };

    const deviceId = 'd3gnmJejkPwfwD7LNsc3pa:APA91bE-XxEHCu-g9kjfxVIBJnfrlA5ibaTfBNKp30inKCRL0FWsehwUKv6Z9LFAulPkQGab1110Rl0NxljymW2N58aWNPQYvHdUTrsqZEd5AxBZqAcVjPkRm5YlcGezShrjEh4J1j3X'


    let lastblock = 0;

    return {
        async checkLastBlock() {
            let block = await web3.eth.getBlock('latest');

            if(lastblock == block.number){
                return;
            }

            console.log(`[*] Searching block ${ block.number }...`);

            if (block && block.transactions) {
                lastblock = block.number;
                let nbTx = 0;
                for (let tx of block.transactions) {
                    nbTx +=1;
                }
                console.log(`[*] ${ nbTx } txs in block ${ block.number }...`);

                for (let tx of block.transactions) {
                    let transaction = await web3.eth.getTransaction(tx);
                    if(transaction !== null && transaction.from !== null)  {
                        if (accounts.includes(transaction.from.toLowerCase())) {
                            var txEtherscanLink = "https://etherscan.io/address/" + transaction.from;
                            fetch('https://fcm.googleapis.com/fcm/send', {
                                method: 'POST',
                                headers: headers,
                                body: JSON.stringify({ "notification": { "title": `${transaction.from}`, "click_action": `${txEtherscanLink}`, }, "to": deviceId })
                            });
                            console.log('\x07');
                            console.log({ address: transaction.from, timestamp: new Date() });
                        }
                    }
                }
            }
        }       
    }
}

