'use strict';
var request = require('request')

module.exports = ({ web3, web3http }) => {

    const accounts = [
        '0x4DbE965AbCb9eBc4c6E9d95aEb631e5B58E70d5b'.toLowerCase(),
        '0x5136a9A5D077aE4247C7706b577F77153C32A01C'.toLowerCase(),
        '0x0BFb53cBF2A5481720fe364Aa7aFd65d9F9183bc'.toLowerCase(),
        '0x198E18EcFdA347c6cdaa440E22b2ff89eaA2cB6f'.toLowerCase(),
        '0x104B2618231020b4320580b40FE05dce77323fa1'.toLowerCase(),
        '0xdac17f958d2ee523a2206206994597c13d831ec7'.toLowerCase()
    ];
    const subscription = web3.eth.subscribe('pendingTransactions', (err, res) => {
        if (err) console.error(err);
    });

    return function watchTransactions() {
            console.log('[+] Watching transactions...');
            subscription.on('data', (txHash) => {
                setTimeout(async () => {
                    try {
                        let tx = await web3http.eth.getTransaction(txHash);
                        if (tx.to != null) {
                            if (accounts.includes(tx.from.toLowerCase())) {
                                console.log('\x07');
                                await new Promise(r => setTimeout(r, 1000));
                                console.log({ address: tx.from, timestamp: new Date() });
                            }
                        }
                    } catch (err) {
                        //console.error(err);
                    }
                }, 1000); //every s
            });
        }
}